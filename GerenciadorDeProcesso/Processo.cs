﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GerenciadorDeProcesso
{
    class Processo
    {
        public string Identificador { get; set; }
        public int Prioridade { get; set; }
        public int Tempo { get; set; }
        public int Tamanho { get; set; }
        public int Chegada { get; set; }

    }
}
