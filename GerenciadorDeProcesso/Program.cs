﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GerenciadorDeProcesso
{ 
    class Program
    {
        private static int numeroDeProcessos;

        static void Main(string[] args)
        {
            Console.WriteLine("Digite o número de Processos:");
            numeroDeProcessos = int.Parse(Console.ReadLine());
            List<Processo> lstProcesso = new List<Processo>();
            List<Processo> lstProcessoPorPrioridade = new List<Processo>();
            List<Processo> lstProcessoPorChegada = new List<Processo>();
            List<Processo> lstProcessoPorTamanho = new List<Processo>();
            string entrada = string.Empty;

            if (numeroDeProcessos > 0)
            {
                Console.WriteLine("Identificador,prioridade,tempo de execução,tamanho,chegada");
                StreamWriter sw = new StreamWriter("ResultadosGerenciamentoDeProcesso.txt");
                sw.WriteLine("Identificador,prioridade,tempo de execução,tamanho,chegada");
                sw.WriteLine($"Entrada:");
                for (int i = 0; i < numeroDeProcessos; i++)
                {
                    Console.WriteLine($"Processo {i}");
                    entrada = Console.ReadLine();
                    sw.WriteLine($"{entrada}");
                    Processo processo = PreencherProcesso(entrada);
                    lstProcesso.Add(processo);
                }

                lstProcessoPorChegada = PorChegada(lstProcesso);
                lstProcessoPorPrioridade = PorPrioridade(lstProcesso);
                lstProcessoPorTamanho = PorTamanho(lstProcesso);


                sw.WriteLine("Resultado");

                sw.WriteLine($"Por Prioridade: primeiro prioridade, depois chegada, depois tamanho;");
                Console.WriteLine("Por Prioridade: primeiro prioridade, depois chegada, depois tamanho;");
                foreach (Processo oProcesso in lstProcessoPorPrioridade)
                {
                    Console.WriteLine(oProcesso.Identificador);
                    sw.WriteLine(oProcesso.Identificador);
                }

                sw.WriteLine($"Por Chegada: somente chegada;");
                Console.WriteLine("Por Chegada: somente chegada;");
                foreach (Processo oProcesso in lstProcessoPorChegada)
                {
                    Console.WriteLine(oProcesso.Identificador);
                    sw.WriteLine(oProcesso.Identificador);
                }

                sw.WriteLine($"Por Tamanho: primeiro chegada, depois tamanho;");
                Console.WriteLine("Por Tamanho: primeiro chegada, depois tamanho;");
                foreach (Processo oProcesso in lstProcessoPorTamanho)
                {
                    Console.WriteLine(oProcesso.Identificador);
                    sw.WriteLine(oProcesso.Identificador);
                }
                sw.Close();
            }
        }
        private static List<Processo> PorTamanho(List<Processo> lstProcesso)
        {
            List<Processo> lstProcessos = lstProcesso.OrderBy(x => x.Chegada).ThenBy(x => x.Tamanho).ToList();
            return lstProcessos;
        }

        private static List<Processo> PorPrioridade(List<Processo> lstProcesso)
        {
            List<Processo> lstProcessos = lstProcesso.OrderByDescending(x => x.Prioridade).ThenBy(x => x.Chegada).ThenBy(x => x.Tamanho).ToList();
            return lstProcessos;
        }

        private static List<Processo> PorChegada(List<Processo> lstProcesso)
        {
            List<Processo> lstProcessos = lstProcesso.OrderBy(x => x.Chegada).ToList();
            return lstProcessos;
        }

        public static Processo PreencherProcesso(string entrada)
        {
            Processo oProcesso = new Processo();
            oProcesso.Identificador = entrada.Split(',')[0];
            oProcesso.Prioridade = int.Parse(entrada.Split(',')[1]);
            oProcesso.Tempo = int.Parse(entrada.Split(',')[2]);
            oProcesso.Tamanho = int.Parse(entrada.Split(',')[3]);
            oProcesso.Chegada = int.Parse(entrada.Split(',')[4]);
            return oProcesso;
        }
    }
}
